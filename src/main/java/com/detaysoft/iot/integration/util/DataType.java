package com.detaysoft.iot.integration.util;

/**
 * Created by Detay on 29.09.2017.
 */
public enum DataType {

    Energy("Enerji"),
    EngineTemperature("Motor Sicakligi"),
    Humidity("Nem"),
    LaserData("LaserData"),
    Round("Tur"),
    Vibration("Titresim");

    private String name;

    DataType(String url) {
        this.name = url;
    }

    public String getName() {
        return name;
    }
}
