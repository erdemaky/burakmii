package com.detaysoft.iot.integration.component;

import com.detaysoft.iot.integration.entity.*;
import com.detaysoft.iot.integration.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.detaysoft.iot.integration.service.HanaDataService;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Detay on 28.09.2017.
 */
@Component
public class IOTCronJob {

	@Autowired
	HanaDataService hanaDataService;

	@Autowired
	IOTEnergyDataRepository iotEnergyDataRepository;

	@Autowired
	IOTEngineTemperatureDataRepository iotEngineTemperatureDataRepository;

	@Autowired
	IOTHumidityDataRepository iotHumidityDataRepository;

	@Autowired
	IOTLaserDataRepository iotLaserDataRepository;

	@Autowired
	IOTRoundDataRepository iotRoundDataRepository;

	@Autowired
	IOTVibrationDataRepository iotVibrationDataRepository;

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncEnergyData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncEnergyData Update");
			List<IOTEnergyData> iotEnergyData = hanaDataService.getEnergyData();
			iotEnergyDataRepository.save(iotEnergyData);

			System.out.println("(" + new Date().toString() + ")END: syncEnergyData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncEngineTemperatureData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncEngineTemperatureData Update");
			List<IOTEngineTemperatureData> iotEngineTemperatureData = hanaDataService.getEngineTemperature();
			iotEngineTemperatureDataRepository.save(iotEngineTemperatureData);

			System.out.println("(" + new Date().toString() + ")END: syncEngineTemperatureData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncHumidityData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncHumidityData Update");
			List<IOTHumidityData> iotHumidityData = hanaDataService.getHumidityData();
			iotHumidityDataRepository.save(iotHumidityData);

			System.out.println("(" + new Date().toString() + ")END: syncHumidityData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncLaserData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncLaserData Update");
			List<IOTLaserData> iotLaserData = hanaDataService.getLaserData();
			iotLaserDataRepository.save(iotLaserData);

			System.out.println("(" + new Date().toString() + ")END: syncLaserData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncRoundData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncRoundData Update");
			List<IOTRoundData> iotRoundData = hanaDataService.getRoundData();
			iotRoundDataRepository.save(iotRoundData);

			System.out.println("(" + new Date().toString() + ")END: syncRoundData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	@Scheduled(initialDelay = 60, fixedRate = 30000)
	@Async
	public void syncVibrationData() {
		try {
			System.out.println("(" + new Date().toString() + ")START: syncVibrationData Update");
			List<IOTVibrationData> iotVibrationData = hanaDataService.getVibrationData();
			iotVibrationDataRepository.save(iotVibrationData);

			System.out.println("(" + new Date().toString() + ")END: syncVibrationData Update");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println();
	}

}
