package com.detaysoft.iot.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@ComponentScan("com.detaysoft.iot.integration")
public class IotIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(IotIntegrationApplication.class, args);
	}
}
