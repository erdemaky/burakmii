package com.detaysoft.iot.integration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IOTHumidityData {

	private String gDevice;
	private String gCreated;
	private String gTemperature;
	private String gHumidity;
	private String gSensorId;
	private String gSignalLevel;
	private String gBatteryLevel;

	@Id
	private String gCreateDate;

	public String getgDevice() {
		return gDevice;
	}

	public void setgDevice(String gDevice) {
		this.gDevice = gDevice;
	}

	public String getgCreated() {
		return gCreated;
	}

	public void setgCreated(String gCreated) {
		this.gCreated = gCreated;
	}

	public String getgTemperature() {
		return gTemperature;
	}

	public void setgTemperature(String gTemperature) {
		this.gTemperature = gTemperature;
	}

	public String getgHumidity() {
		return gHumidity;
	}

	public void setgHumidity(String gHumidity) {
		this.gHumidity = gHumidity;
	}

	public String getgSensorId() {
		return gSensorId;
	}

	public void setgSensorId(String gSensorId) {
		this.gSensorId = gSensorId;
	}

	public String getgSignalLevel() {
		return gSignalLevel;
	}

	public void setgSignalLevel(String gSignalLevel) {
		this.gSignalLevel = gSignalLevel;
	}

	public String getgBatteryLevel() {
		return gBatteryLevel;
	}

	public void setgBatteryLevel(String gBatteryLevel) {
		this.gBatteryLevel = gBatteryLevel;
	}

	public String getgCreateDate() {
		return gCreateDate;
	}

	public void setgCreateDate(String gCreateDate) {
		this.gCreateDate = gCreateDate;
	}

}
