package com.detaysoft.iot.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.iot.integration.entity.IOTHumidityData;

public interface IOTHumidityDataRepository extends CrudRepository<IOTHumidityData, Long>{

}
