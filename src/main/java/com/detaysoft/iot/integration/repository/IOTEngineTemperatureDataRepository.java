package com.detaysoft.iot.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.iot.integration.entity.IOTEngineTemperatureData;

public interface IOTEngineTemperatureDataRepository extends CrudRepository<IOTEngineTemperatureData, Long> {

}
