package com.detaysoft.iot.integration.repository;

import com.detaysoft.iot.integration.entity.IOTLaserData;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Detay on 28.09.2017.
 */
public interface IOTLaserDataRepository extends CrudRepository<IOTLaserData,Long>{

}
