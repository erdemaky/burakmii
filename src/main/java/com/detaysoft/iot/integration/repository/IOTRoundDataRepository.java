package com.detaysoft.iot.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.iot.integration.entity.IOTRoundData;

public interface IOTRoundDataRepository extends CrudRepository<IOTRoundData, Long>{

}
