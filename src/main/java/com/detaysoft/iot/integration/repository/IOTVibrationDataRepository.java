package com.detaysoft.iot.integration.repository;

import org.springframework.data.repository.CrudRepository;

import com.detaysoft.iot.integration.entity.IOTVibrationData;

public interface IOTVibrationDataRepository extends CrudRepository<IOTVibrationData, Long>{

}
