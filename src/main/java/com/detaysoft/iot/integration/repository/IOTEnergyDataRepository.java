package com.detaysoft.iot.integration.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.core.EntityInformation;

import com.detaysoft.iot.integration.entity.IOTEnergyData;

public interface IOTEnergyDataRepository extends CrudRepository<IOTEnergyData, Long> {
	
	
}
