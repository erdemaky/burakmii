package com.detaysoft.iot.integration.service;

import com.detaysoft.iot.integration.entity.*;

import java.util.List;

/**
 * Created by Detay on 28.09.2017.
 */

public interface HanaDataService {

    public List<IOTEnergyData> getEnergyData();

    public List<IOTEngineTemperatureData> getEngineTemperature();

    public List<IOTHumidityData> getHumidityData();

    public List<IOTLaserData> getLaserData();

    public List<IOTRoundData> getRoundData();

    public List<IOTVibrationData> getVibrationData();

}
