package com.detaysoft.iot.integration.service.impl;

import com.detaysoft.iot.integration.entity.*;
import com.detaysoft.iot.integration.service.HanaDataService;
import com.detaysoft.iot.integration.util.DataType;
import com.hazelcast.util.StringUtil;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Detay on 28.09.2017.
 */
@Service
public class HanaDataServiceImpl implements HanaDataService {

    @Override
    public List<IOTEnergyData> getEnergyData() {
        try {
            NodeList nList = getData(DataType.Energy);

            List<IOTEnergyData> iotEnergyDatas = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gCurrent = mPropElement.getElementsByTagName("d:C_CURRENT").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTEnergyData iotEnergyData = new IOTEnergyData();

                iotEnergyData.setgDevice(gDevice);
                iotEnergyData.setgCreated(changeTimeZone(gCreated));
                iotEnergyData.setgCurrent(gCurrent);
                iotEnergyData.setgSensorId(gSensorId);
                iotEnergyData.setgSignalLevel(gSignalLevel);
                iotEnergyData.setgBatteryLevel(gBatteryLevel);
                iotEnergyData.setgCreateDate(changeTimeZone(gCreateDate));

                iotEnergyDatas.add(iotEnergyData);
            }

            Collections.reverse(iotEnergyDatas);

            return iotEnergyDatas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<IOTEngineTemperatureData> getEngineTemperature() {
        try {
            NodeList nList = getData(DataType.EngineTemperature);

            List<IOTEngineTemperatureData> iotEngineTemperatureDatas = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gTemperature = mPropElement.getElementsByTagName("d:C_TEMPERATURE").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTEngineTemperatureData iotEngineTemperatureData = new IOTEngineTemperatureData();

                iotEngineTemperatureData.setgDevice(gDevice);
                iotEngineTemperatureData.setgCreated(changeTimeZone(gCreated));
                iotEngineTemperatureData.setgTemperature(gTemperature);
                iotEngineTemperatureData.setgSensorId(gSensorId);
                iotEngineTemperatureData.setgSignalLevel(gSignalLevel);
                iotEngineTemperatureData.setgBatteryLevel(gBatteryLevel);
                iotEngineTemperatureData.setgCreateDate(changeTimeZone(gCreateDate));

                iotEngineTemperatureDatas.add(iotEngineTemperatureData);
            }


            return iotEngineTemperatureDatas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<IOTHumidityData> getHumidityData() {
        try {
            NodeList nList = getData(DataType.Humidity);

            List<IOTHumidityData> iotHumidityDatas = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gTemperature = mPropElement.getElementsByTagName("d:C_TEMPERATURE").item(0).getTextContent();
                String gHumidity = mPropElement.getElementsByTagName("d:C_HUMIDITY").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTHumidityData iotHumidityData = new IOTHumidityData();

                iotHumidityData.setgDevice(gDevice);
                iotHumidityData.setgCreated(changeTimeZone(gCreated));
                iotHumidityData.setgTemperature(gTemperature);
                iotHumidityData.setgHumidity(gHumidity);
                iotHumidityData.setgSensorId(gSensorId);
                iotHumidityData.setgSignalLevel(gSignalLevel);
                iotHumidityData.setgBatteryLevel(gBatteryLevel);
                iotHumidityData.setgCreateDate(changeTimeZone(gCreateDate));

                iotHumidityDatas.add(iotHumidityData);
            }

            Collections.reverse(iotHumidityDatas);

            return iotHumidityDatas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<IOTLaserData> getLaserData() {
        try {
            NodeList nList = getData(DataType.LaserData);

            List<IOTLaserData> iotLaserDataList = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gLaserCounter = mPropElement.getElementsByTagName("d:C_LASERCOUNTER").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTLaserData iotLaserData = new IOTLaserData();

                iotLaserData.setgDevice(gDevice);
                iotLaserData.setgCreated(changeTimeZone(gCreated));
                iotLaserData.setgLaserCounter(gLaserCounter);
                iotLaserData.setgSensorId(gSensorId);
                iotLaserData.setgSignalLevel(gSignalLevel);
                iotLaserData.setgBatteryLevel(gBatteryLevel);
                iotLaserData.setgCreateDate(changeTimeZone(gCreateDate));

                iotLaserDataList.add(iotLaserData);
            }

            Collections.reverse(iotLaserDataList);

            return iotLaserDataList;
        } catch (Exception e) {

            return null;
        }
    }

    @Override
    public List<IOTRoundData> getRoundData() {
        try {
            NodeList nList = getData(DataType.Round);

            List<IOTRoundData> iotRoundDatas = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gRoute = mPropElement.getElementsByTagName("d:C_ROUTE").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTRoundData iotRoundData = new IOTRoundData();

                iotRoundData.setgDevice(gDevice);
                iotRoundData.setgCreated(changeTimeZone(gCreated));
                iotRoundData.setgRoute(gRoute);
                iotRoundData.setgSensorId(gSensorId);
                iotRoundData.setgSignalLevel(gSignalLevel);
                iotRoundData.setgBatteryLevel(gBatteryLevel);
                iotRoundData.setgCreateDate(changeTimeZone(gCreateDate));

                iotRoundDatas.add(iotRoundData);
            }

            Collections.reverse(iotRoundDatas);

            return iotRoundDatas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<IOTVibrationData> getVibrationData() {
        try {
            NodeList nList = getData(DataType.Vibration);

            List<IOTVibrationData> iotVibrationDatas = new ArrayList<>();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;

                Node mProp = eElement.getElementsByTagName("content").item(0).getFirstChild();
                Element mPropElement = (Element) mProp;

                String gDevice = mPropElement.getElementsByTagName("d:G_DEVICE").item(0).getTextContent();
                String gCreated = mPropElement.getElementsByTagName("d:G_CREATED").item(0).getTextContent();
                String gVibration = mPropElement.getElementsByTagName("d:C_VIBRATION").item(0).getTextContent();
                String gSensorId = mPropElement.getElementsByTagName("d:C_SENSORID").item(0).getTextContent();
                String gSignalLevel = mPropElement.getElementsByTagName("d:C_SIGNALLEVEL").item(0).getTextContent();
                String gBatteryLevel = mPropElement.getElementsByTagName("d:C_BATTERYLEVEL").item(0).getTextContent();
                String gCreateDate = mPropElement.getElementsByTagName("d:C_CREATEDATE").item(0).getTextContent();

                IOTVibrationData iotVibrationData = new IOTVibrationData();

                iotVibrationData.setgDevice(gDevice);
                iotVibrationData.setgCreated(changeTimeZone(gCreated));
                iotVibrationData.setgVibration(gVibration);
                iotVibrationData.setgSensorId(gSensorId);
                iotVibrationData.setgSignalLevel(gSignalLevel);
                iotVibrationData.setgBatteryLevel(gBatteryLevel);
                iotVibrationData.setgCreateDate(changeTimeZone(gCreateDate));

                iotVibrationDatas.add(iotVibrationData);
            }

            Collections.reverse(iotVibrationDatas);

            return iotVibrationDatas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getServiceURL(DataType dataType) {
        if (dataType.equals(DataType.LaserData)) {
            return "https://iotmmsa0cf5bf81.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_39B2D991CD16D91D486E";
        } else if (dataType.equals(DataType.EngineTemperature)) {
            return "https://iotmmsa0cf5bf81.cert.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_DD2DC64268515A5FC31C";
        } else if (dataType.equals(DataType.Energy)) {
            return "https://iotmmsa0cf5bf81.cert.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_56B201C4A0974F3BA91D";
        } else if (dataType.equals(DataType.Humidity)) {
            return "https://iotmmsa0cf5bf81.cert.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_C9CD21F0A74125E6A33B";
        } else if (dataType.equals(DataType.Round)) {
            return "https://iotmmsa0cf5bf81.cert.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_590235EB2C8EB3C1CE06";
        } else if (dataType.equals(DataType.Vibration)) {
            return "https://iotmmsa0cf5bf81.cert.hana.ondemand.com/com.sap.iotservices.mms/v1/api/http/app.svc/NEO_APVTE1OJQKIRRCKPYGGGOH7IF.T_IOT_3FB426B1ED610CC320E2";
        } else {
            return null;
        }
    }

    private String changeTimeZone(String gCreated) throws ParseException{
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC+3"));

        Date date = formatter.parse(gCreated);
        return date.toString();
    }

    private NodeList getData(DataType dataType) throws Exception {
        String url = getServiceURL(dataType);

        if (StringUtil.isNullOrEmpty(url))
            return null;

        String xmlString = getHTTPRequest(url);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new InputSource(new StringReader(xmlString)));


        return doc.getElementsByTagName("entry");
    }

    private String getHTTPRequest(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        con.setDoOutput(true);
        con.setRequestProperty("Authorization", "Basic YnVyYWtlcGlrQGdtYWlsLmNvbTpTYXA3NzMyNDMzKg==");
        con.setRequestProperty("Content-Type", "application/atom+xml");
        con.setRequestProperty("accept", "*/*");

//        int responseCode = con.getResponseCode();

        InputStream is;
        if (con.getResponseCode() >= 400) {
            is = con.getErrorStream();
        } else {
            is = con.getInputStream();
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}

